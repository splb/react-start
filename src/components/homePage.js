"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;

var Home = React.createClass({
	render: function() {
		return (
			<div className = "jumbotron">
				<h1>Home page</h1>
				<p>This is my home page. There are many like it, but this one is mine.</p>
				<p>My home page is my best friend. It is my life. I must master it as I must master my life.</p>
				<p>My home page, without me, is useless. Without my home page, I am useless.</p>
				<Link to="about" className="btn btn-primary btn-lg">Learn more</Link>
			</div>
		);
	}
});

module.exports = Home;