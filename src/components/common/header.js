"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;

var logoStyle = {
	width: '25px',
	marginRight: '15px'
};

var Header = React.createClass({
	render: function() {
		return (
			<nav className="navbar navbar-default">
				<div className="container-fluid">
					<Link to="app" className="navbar-brand">
						<div><img src="images/react.svg" style={logoStyle}/>Built with React</div>
					</Link>
					<ul className="nav navbar-nav">
						<li><Link to="app">Home</Link></li>
						<li><Link to="authors">Authors</Link></li>
						<li><Link to="about">About</Link></li>
					</ul>
				</div>
			</nav>
		);
	}
});

module.exports = Header;