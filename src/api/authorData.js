module.exports = {
	authors: 
	[
		{
			id: 'tess-ting', 
			firstName: 'Tess', 
			lastName: 'Ting'
		},	
		{
			id: 'jason-stringe', 
			firstName: 'Jason', 
			lastName: 'Stringe'
		}
	]
};